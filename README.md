PyQT控制机械臂的运动相关资源。
* MyModel:FreeCAD和Coppeliasim的相关模型资源。
* PyQTUI：pyQT的UI文件
* ArmPositionControlTest.py：pyqt代码文件，也是运行的主文件。
* MyArmTest.py：机械臂操控相关代码。可独立运行用于测试。

运行方式：
* Coppeliasim中打开CoppeliaSimProject.ttt模型文件，点击运行。
* Spyder中打开ArmPositionControlTest.py和MyArmTest.py。其中ArmPositionControlTest是运行pyqt的主文件，主要运行这个，MyArmTest是机械臂操控相关的代码，也可以直接运行测试程序。
目前程序没有完善，后续会完善。
